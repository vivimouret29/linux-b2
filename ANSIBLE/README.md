# ANSIBLE

## Suivi du td

L'inventaire est réalisé sans problème, on voit le fichier `hosts` ci-dessous.

```bash
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ cat inventory/hosts 
[target]
192.168.56.41 hostname=target-1
192.168.56.42 hostname=target-2

[target:vars]
variabledetarget=lavaleure
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ ansible -i ./inventory/ target -a "echo hello" -u ansible
192.168.56.41 | SUCCESS | rc=0 >>
hello

192.168.56.42 | SUCCESS | rc=0 >>
hello

(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ ansible -i ./inventory/ target -a "id" -u ansible --become
192.168.56.41 | SUCCESS | rc=0 >>
uid=0(root) gid=0(root) groups=0(root) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023

192.168.56.42 | SUCCESS | rc=0 >>
uid=0(root) gid=0(root) groups=0(root) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
```

Le module copy, je passe de l'hôte à une vagrant pour chaque commande : 
```bash
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ ansible -i ./inventory/ target -m copy -a "src=./inventory/hosts dest=/tmp/hosts" -u ansible
192.168.56.42 | SUCCESS => {
    "changed": true, 
    "checksum": "b5b81939ed1c741ccb947f6b96e44982959ad32f", 
    "dest": "/tmp/hosts", 
    "gid": 1001, 
    "group": "ansible", 
    "md5sum": "761c52e985e2eb76776fe6a7914bc795", 
    "mode": "0664", 
    "owner": "ansible", 
    "secontext": "unconfined_u:object_r:user_home_t:s0", 
    "size": 115, 
    "src": "/home/ansible/.ansible/tmp/ansible-tmp-1585305995.55-80930715454834/source", 
    "state": "file", 
    "uid": 1001
}
192.168.56.41 | SUCCESS => {
    "changed": true, 
    "checksum": "b5b81939ed1c741ccb947f6b96e44982959ad32f", 
    "dest": "/tmp/hosts", 
    "gid": 1001, 
    "group": "ansible", 
    "md5sum": "761c52e985e2eb76776fe6a7914bc795", 
    "mode": "0664", 
    "owner": "ansible", 
    "secontext": "unconfined_u:object_r:user_home_t:s0", 
    "size": 115, 
    "src": "/home/ansible/.ansible/tmp/ansible-tmp-1585305995.55-131247221909361/source", 
    "state": "file", 
    "uid": 1001
}
[ansible@target-2 ~]$ cat /tmp/hosts 
[target]
192.168.56.41 hostname=target-1
192.168.56.42 hostname=target-2

[target:vars]
variabledetarget=lavaleure

(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ ansible -i ./inventory/ target -m file -a "dest=/tmp/hosts mode=600 owner=ansible state=file" -u ansible --become
192.168.56.42 | SUCCESS => {
    "changed": true, 
    "gid": 1001, 
    "group": "ansible", 
    "mode": "0600", 
    "owner": "ansible", 
    "path": "/tmp/hosts", 
    "secontext": "unconfined_u:object_r:user_home_t:s0", 
    "size": 115, 
    "state": "file", 
    "uid": 1001
}
192.168.56.41 | SUCCESS => {
    "changed": true, 
    "gid": 1001, 
    "group": "ansible", 
    "mode": "0600", 
    "owner": "ansible", 
    "path": "/tmp/hosts", 
    "secontext": "unconfined_u:object_r:user_home_t:s0", 
    "size": 115, 
    "state": "file", 
    "uid": 1001
}
[ansible@target-2 ~]$ ll /tmp/ 
total 8
-rw-------. 1 ansible ansible  115 Mar 27 10:46 hosts
drwx------. 3 root    root      17 Mar 27 09:43 systemd-private-51398ff561354117a646f2a1c0c4c8c2-chronyd.service-iVbNoa
-rwx--x--x. 1 vagrant vagrant 1060 Mar 27 09:43 vagrant-shell


192.168.56.42 | SUCCESS => {
    "changed": true, 
    "msg": "", 
    "rc": 0, 
    "results": [
        "Loaded plugins: fastestmirror\nLoading mirror speeds from cached hostfile\n * base: mirrors.ircam.fr\n * extras: ftp.pasteur.fr\n * updates: centos.mirrors.proxad.net\nResolving Dependencies\n--> Running transaction check\n---> Package epel-release.noarch 0:7-11 will be installed\n--> Finished Dependency Resolution\n\nDependencies Resolved\n\n================================================================================\n Package                Arch             Version         Repository        Size\n================================================================================\nInstalling:\n epel-release           noarch           7-11            extras            15 k\n\nTransaction Summary\n================================================================================\nInstall  1 Package\n\nTotal download size: 15 k\nInstalled size: 24 k\nDownloading packages:\nRunning transaction check\nRunning transaction test\nTransaction test succeeded\nRunning transaction\n  Installing : epel-release-7-11.noarch                                     1/1 \n  Verifying  : epel-release-7-11.noarch                                     1/1 \n\nInstalled:\n  epel-release.noarch 0:7-11                                                    \n\nComplete!\n"
    ]
}......

(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ ansible -i ./inventory/ target -m service -a "name=httpd state=restarted" -u ansible --become
192.168.56.41 | FAILED! => {
    "changed": false, 
    "msg": "Could not find the requested service httpd: host"
}
192.168.56.42 | FAILED! => {
    "changed": false, 
    "msg": "Could not find the requested service httpd: host"
}
```

Il manque les packages d'httpd 

```bash
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ ansible -i ./inventory/ target -m yum -a "name=httpd state=present" -u ansible --become
192.168.56.41 | SUCCESS => {
    "changed": true, 
    "msg": "", 
    "rc": 0, 
    "results": [
        "Loaded plugins: fastestmirror\nLoading mirror speeds from cached hostfile\n * base: centos.crazyfrogs.org\n * epel: mirrors.coreix.net\n * extras: ftp.pasteur.fr\n * updates: centos.crazyfrogs.org\nResolving Dependencies\n--> Running transaction check\n---> Package httpd.x86_64 0:2.4.6-90.el7.centos will be installed\n--> Processing Dependency: httpd-tools = 2.4.6-90.el7.centos for package: httpd-2.4.6-90.el7.centos.x86_64\n--> Processing Dependency: system-logos >= 7.92.1-1 for package: httpd-2.4.6-90.el7.centos.x86_64\n--> Processing Dependency: /etc/mime.types for package: httpd-2.4.6-90.el7.centos.x86_64\n--> Processing Dependency: libaprutil-1.so.0()(64bit) for package: httpd-2.4.6-90.el7.centos.x86_64\n--> Processing Dependency: libapr-1.so.0()(64bit) for package: httpd-2.4.6-90.el7.centos.x86_64\n--> Running transaction check\n---> Package apr.x86_64 0:1.4.8-5.el7 will be installed\n---> Package apr-util.x86_64 0:1.5.2-6.el7 will be installed\n---> Package centos-logos.noarch 0:70.0.6-3.el7.centos will be installed\n---> Package httpd-tools.x86_64 0:2.4.6-90.el7.centos will be installed\n---> Package mailcap.noarch 0:2.1.41-2.el7 will be installed\n--> Finished Dependency Resolution\n\nDependencies Resolved\n\n================================================================================\n Package             Arch          Version                    Repository   Size\n================================================================================\nInstalling:\n httpd               x86_64        2.4.6-90.el7.centos        base        2.7 M\nInstalling for dependencies:\n apr                 x86_64        1.4.8-5.el7                base        103 k\n apr-util            x86_64        1.5.2-6.el7                base         92 k\n centos-logos        noarch        70.0.6-3.el7.centos        base         21 M\n httpd-tools         x86_64        2.4.6-90.el7.centos        base         91 k\n mailcap             noarch        2.1.41-2.el7               base         31 k\n\nTransaction Summary\n================================================================================\nInstall  1 Package (+5 Dependent packages)\n\nTotal download size: 24 M\nInstalled size: 31 M\nDownloading packages:\n--------------------------------------------------------------------------------\nTotal                                              5.8 MB/s |  24 MB  00:04     \nRunning transaction check\nRunning transaction test\nTransaction test succeeded\nRunning transaction\n  Installing : apr-1.4.8-5.el7.x86_64                                       1/6 \n  Installing : apr-util-1.5.2-6.el7.x86_64                                  2/6 \n  Installing : httpd-tools-2.4.6-90.el7.centos.x86_64                       3/6 \n  Installing : centos-logos-70.0.6-3.el7.centos.noarch                      4/6 \n  Installing : mailcap-2.1.41-2.el7.noarch                                  5/6 \n  Installing : httpd-2.4.6-90.el7.centos.x86_64                             6/6 \n  Verifying  : mailcap-2.1.41-2.el7.noarch                                  1/6 \n  Verifying  : httpd-tools-2.4.6-90.el7.centos.x86_64                       2/6 \n  Verifying  : apr-util-1.5.2-6.el7.x86_64                                  3/6 \n  Verifying  : httpd-2.4.6-90.el7.centos.x86_64                             4/6 \n  Verifying  : apr-1.4.8-5.el7.x86_64                                       5/6 \n  Verifying  : centos-logos-70.0.6-3.el7.centos.noarch                      6/6 \n\nInstalled:\n  httpd.x86_64 0:2.4.6-90.el7.centos                                            \n\nDependency Installed:\n  apr.x86_64 0:1.4.8-5.el7                                                      \n  apr-util.x86_64 0:1.5.2-6.el7                                                 \n  centos-logos.noarch 0:70.0.6-3.el7.centos                                     \n  httpd-tools.x86_64 0:2.4.6-90.el7.centos                                      \n  mailcap.noarch 0:2.1.41-2.el7                                                 \n\nComplete!\n"
    ]
}...
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ ansible -i ./inventory/ target -m service -a "name=httpd state=restarted" -u ansible --become
192.168.56.41 | SUCCESS => {
    "changed": true, 
    "name": "httpd", 
    "state": "started", 
    "status": {
        "ActiveEnterTimestampMonotonic": "0", 
        "ActiveExitTimestampMonotonic": "0", 
        "ActiveState": "inactive", 
        "After": "network.target basic.target -.mount system.slice nss-lookup.target remote-fs.target tmp.mount systemd-journald.socket", 
        "AllowIsolate": "no", 
        "AmbientCapabilities": "0",
```

Test Playbook
```bash
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ ansible-playbook -i inventory apache-base-config.yml

PLAY [apache base config] *****************************************************************************************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************************************************************************************
ok: [192.168.56.41]
ok: [192.168.56.42]

TASK [ensure apache is up to date] ********************************************************************************************************************************************************************************
ok: [192.168.56.41]
ok: [192.168.56.42]

TASK [write the apache config file] *******************************************************************************************************************************************************************************
changed: [192.168.56.41]
changed: [192.168.56.42]

TASK [ensure apache is running] ***********************************************************************************************************************************************************************************
ok: [192.168.56.42]
ok: [192.168.56.41]

RUNNING HANDLER [restart apache] **********************************************************************************************************************************************************************************
changed: [192.168.56.42]
changed: [192.168.56.41]

PLAY RECAP ********************************************************************************************************************************************************************************************************
192.168.56.41              : ok=5    changed=2    unreachable=0    failed=0   
192.168.56.42              : ok=5    changed=2    unreachable=0    failed=0
```

J'ai eu des changed, j'avais bêtement mis le fichier `httpd.j2` dans le dossier courant et pas dans templates.

Maintenant les rôles :

```bash
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ mkdir roles && mkdir roles/apache-base-config && mkdir roles/apache-base-config/defaults && mkdir roles/apache-base-config/files && mkdir roles/apache-base-config/handlers && mkdir roles/apache-base-config/meta && mkdir roles/apache-base-config/tasks && mkdir roles/apache-base-config/templates && mkdir roles/apache-base-config/vars
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ tree roles
roles
└── apache-base-config
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    ├── tasks
    │   └── main.yml
    ├── templates
    │   └── httpd.j2
    └── vars
        └── main.yml

8 directories, 5 files
```

Création du rôle apache :
```bash
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ tree roles
roles
└── apache-base-config
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    ├── tasks
    │   └── main.yml
    ├── templates
    │   └── httpd.j2
    └── vars
        └── main.yml

8 directories, 5 files
(base) vivien@localviv:~/Ynov/linux-b2/ANSIBLE$ ansible-playbook -i inventory apache-base-config.yml 

PLAY [apache base config] ************************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************
ok: [192.168.56.41]
ok: [192.168.56.42]

TASK [apache-base-config : ensure apache is up to date] ******************************************************************************************************
ok: [192.168.56.42]
ok: [192.168.56.41]

TASK [apache-base-config : write the apache config file] *****************************************************************************************************
ok: [192.168.56.42]
ok: [192.168.56.41]

TASK [apache-base-config : ensure apache is running] *********************************************************************************************************
ok: [192.168.56.42]
ok: [192.168.56.41]

PLAY RECAP ***************************************************************************************************************************************************
192.168.56.41              : ok=4    changed=0    unreachable=0    failed=0   
192.168.56.42              : ok=4    changed=0    unreachable=0    failed=0
```

