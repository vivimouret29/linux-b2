# DOCKER

## Y'a quoi ici ?

J'ai suivant *totalement* à la trace le td pour bien comprendre ce qu'il se passe.

```bash
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                     PORTS               NAMES
7a5115593664        hello-world         "/hello"                 9 seconds ago       Exited (0) 8 seconds ago                       nifty_pare
12f54cdaa9d1        hello-world         "/hello"                 2 hours ago         Exited (0) 2 hours ago                         condescending_albattani
```
En faisant deux fois, `$ docker run hello-world`, je remarque que même leurs noms sont différents.

```bash
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker rm 12f54cdaa9d1
12f54cdaa9d1
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
7a5115593664        hello-world         "/hello"                 34 seconds ago      Exited (0) 33 seconds ago                       nifty_pare
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker start -a 7a5115593664

Hello from Docker!
This message shows that your installation appears to be working correctly.

vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker run -d alpine sh -c 'echo starting; ; sleep 30 ; echo stop'
Unable to find image 'alpine:latest' locally
latest: Pulling from library/alpine
c9b1b535fdd9: Pull complete 
Digest: sha256:ab00606a42621fb68f2ed6ad3c88be54397f981a7b70a79db3d1172b11c4367d
Status: Downloaded newer image for alpine:latest
b58f0e91c580766fec37f6577e6c6c4d20cda532aa4344e15778cacc9a0148c2
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
8118945c79df        alpine              "sh -c 'echo startin…"   13 seconds ago      Exited (2) 12 seconds ago                       intelligent_buck
b58f0e91c580        alpine              "sh -c 'echo startin…"   2 minutes ago       Exited (2) 2 minutes ago                        frosty_bose
```
Ici, je remarque que mes container n'ont pas été up une seule seconde..
J'ai pensé qu'à côté d'Exited était indiqué le nombre d'erreurs, je sors un `$ docker logs 8118945c79df` et j'ai saisi la faute :
```bash
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker logs 8118945c79df
sh: syntax error: unexpected ";"
```
Puis ça marche tranquillement après.
```bash
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker logs 5e0cb7b449f6
starting
stop
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker start 5e0cb7b449f6
5e0cb7b449f6
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
5e0cb7b449f6        alpine              "sh -c 'echo startin…"   5 minutes ago       Up 19 seconds                                   exciting_haibt
vivien@localviv:~/Ynov/linux-b2/DOCKER$ docker exec -it 16709a45452a sh
/ # 
/ # ps
PID   USER     TIME  COMMAND
    1 root      0:00 sh -c echo lets go boys ; sleep 999 ; echo we finish our jobs bro !
    7 root      0:00 sleep 999
    8 root      0:00 sh
   13 root      0:00 ps
/ # ls
bin    dev    etc    home   lib    media  mnt    opt    proc   root   run    sbin   srv    sys    tmp    usr    var
/ # 
/ # 
```
Là je vois que tu fais une partie **docker rm**, je l'ai utilisé avant par pur instinct.
Et ça n'a aucun rapport, du coup je me demande qu'elle est la différence entre les deux ?
Étant donné que je suis sur ma machine et que j'ai des docker que j'aimerai gardé je ne fais pas la commande `$ docker ps -a | awk '$1~/^[0-9|a-z]*$/ {print "docker rm "$1}' | bash` mais elle m'a l'air très bien pratique sur une vm comme tu l'étais.

## TP ?

Il faut monter un serveur Gitea* en faisant tout soi-même.
Voici mon suivi du tp : 

Pour créer un serveur Gitea sous docker c'est compliqué...
Il faut commencer par importer les librairies
```bash
wget -O gitea https://dl.gitea.io/gitea/1.11.3/gitea-1.11.3-linux-amd64
chmod +x gitea
```

Vérifions les packages et testons le :
```bash
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ gpg --keyserver pgp.mit.edu --recv 7C9E68152594688862D62AF62D9AE806EC1592E2
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ ./gitea web
2020/03/21 19:41:24 cmd/web.go:107:runWeb() [I] Starting Gitea on PID: 28643
2020/03/21 19:41:24 ...dules/setting/git.go:91:newGit() [I] Git Version: 2.17.1
2020/03/21 19:41:24 routers/init.go:87:GlobalInit() [T] AppPath: /home/vivien/Ynov/linux-b2/DOCKER/TP/gitea
2020/03/21 19:41:24 routers/init.go:88:GlobalInit() [T] AppWorkPath: /home/vivien/Ynov/linux-b2/DOCKER/TP
2020/03/21 19:41:24 routers/init.go:89:GlobalInit() [T] Custom path: /home/vivien/Ynov/linux-b2/DOCKER/TP/custom
2020/03/21 19:41:24 routers/init.go:90:GlobalInit() [T] Log path: /home/vivien/Ynov/linux-b2/DOCKER/TP/log
2020/03/21 19:41:24 ...dules/setting/log.go:233:newLogService() [I] Gitea v1.11.3 built with GNU Make 4.1, go1.13.8 : bindata, sqlite, sqlite_unlock_notify
2020/03/21 19:41:24 ...dules/setting/log.go:276:n   ewLogService() [I] Gitea Log Mode: Console(Console:info)
2020/03/21 19:41:24 ...les/setting/cache.go:45:newCacheService() [I] Cache Service Enabled
2020/03/21 19:41:24 ...s/setting/session.go:44:newSessionService() [I] Session Service Enabled
2020/03/21 19:41:24 routers/init.go:122:GlobalInit() [I] SQLite3 Supported
2020/03/21 19:41:24 routers/init.go:46:checkRunMode() [I] Run Mode: Development
2020/03/21 19:41:25 cmd/web.go:161:runWeb() [I] Listen: http://0.0.0.0:3000
2020/03/21 19:41:25 ...s/graceful/server.go:55:NewServer() [I] Starting new server: tcp:0.0.0.0:3000 on PID: 28643
^C2020/03/21 19:41:29 ...eful/manager_unix.go:130:handleSignals() [W] PID 28643. Received SIGINT. Shutting down...
2020/03/21 19:41:29 cmd/web.go:206:runWeb() [I] HTTP Listener: 0.0.0.0:3000 Closed
2020/03/21 19:41:29 .../graceful/manager.go:184:doHammerTime() [W] Setting Hammer condition
2020/03/21 19:41:29 ...eful/server_hooks.go:47:doShutdown() [I] PID: 28643 Listener ([::]:3000) closed.
2020/03/21 19:41:30 .../graceful/manager.go:198:doTerminate() [W] Terminating
2020/03/21 19:41:30 cmd/web.go:208:runWeb() [I] PID: 28643 Gitea Web Finished
```

Créons un user pour utiliser Gitea et créons sa structure :
```bash
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ sudo adduser --system --shel /bin/bash --gecos 'Git Version Control' --group --disabled-password --home /home/git git
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ sudo mkdir -p /var/lib/gitea/custom
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ sudo chown -R git:git /var/lib/gitea/
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ sudo chmod -R 750 /var/lib/gitea/
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ sudo mkdir /etc/gitea
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ sudo chown root:git /etc/gitea
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ sudo chmod 770 /etc/gitea
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ sudo cp gitea /usr/local/bin/gitea
```

Maintenant, il faut nous en remettre à la configuration. 
Pensons à créer son sample disponible sur leurs github.
```bash
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ systemctl enable gitea
Created symlink /etc/systemd/system/multi-user.target.wants/gitea.service → /etc/systemd/system/gitea.service.
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ systemctl start gitea
vivien@localviv:~/Ynov/linux-b2/DOCKER/TP$ systemctl status gitea
● gitea.service - Gitea (Git with a cup of tea)
   Loaded: loaded (/etc/systemd/system/gitea.service; enabled; vendor preset: enabled)
   Active: active (running) since Sat 2020-03-21 20:38:44 CET; 1s ago
 Main PID: 27851 (gitea)
    Tasks: 13 (limit: 4915)
   CGroup: /system.slice/gitea.service
           └─27851 /usr/local/bin/gitea web --config /etc/gitea/app.ini

mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 ...dules/setting/log.go:276:newLogService() [I] Gitea Log Mode: Console(Console:info)
mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 ...les/setting/cache.go:45:newCacheService() [I] Cache Service Enabled
mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 ...s/setting/session.go:44:newSessionService() [I] Session Service Enabled
mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 routers/init.go:122:GlobalInit() [I] SQLite3 Supported
mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 routers/init.go:46:checkRunMode() [I] Run Mode: Development
mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 cmd/web.go:161:runWeb() [I] Listen: http://0.0.0.0:3000
mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 ...s/graceful/server.go:55:NewServer() [I] Starting new server: tcp:0.0.0.0:3000 on PID: 27851
mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 ...s/graceful/server.go:79:ListenAndServe() [E] Unable to GetListener: listen tcp 0.0.0.0:3000: bind: address already in use
mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 cmd/web.go:204:runWeb() [C] Failed to start server: listen tcp 0.0.0.0:3000: bind: address already in use
mars 21 20:38:44 localviv gitea[27851]: 2020/03/21 20:38:44 cmd/web.go:206:runWeb() [I] HTTP Listener: 0.0.0.0:3000 Closed
```

Notre serveur Gitea est bien installé et configuré.
Plus qu'à créer un docker avec le setup Gitea tout neuf.














**(gitea se prononce [/ɡɪ’ti:/](https://youtu.be/EM71-2uDAoY))*