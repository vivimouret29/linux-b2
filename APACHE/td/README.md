# **TD APACHE**

* installation rajouter dans vagrantfile

        yum -y install tree
        yum -y install vim


changer de nom de serveur apache2

* commandes :
```
[root@websrv var]# find / -name httpd.conf
/etc/httpd/conf/httpd.conf
/usr/lib/tmpfiles.d/httpd.conf
[root@websrv var]# vim /etc/httpd/conf/httpd.conf 
```
ce qui nous interèsse est la première ligne

* puis modifcation du fichier :
```
# ServerName gives the name and port that the server uses to identify itself.
# This can often be determined automatically, but we recommend you specify
# it explicitly to prevent problems during startup.
#
# If your host doesn't have a registered DNS name, enter its IP address here.
#
ServerName dmserver:80
```


lien symbolique :

la commande `[root@websrv ~]# ln -s /  /var/www/html/slash` va créer un raccourci dans `/` ce qui permet d'avoir toutes l'arborescence sur le serveur apache...

une fois les droits réécrits, on n'a plus les droits d'accéder à /slash/ 
`403 Forbidden. You don't have permission to access /slash/ on this server.`

modification d'un dossier/fichier

```
[root@websrv ~]# sed 's/Titre nul/Site 1/' /var/www/html/index.html > /var/www/html/site1/index.html
[root@websrv ~]# sed 's/Titre nul/Site 2/' /var/www/html/index.html > /var/www/html/site2/index.html
[root@websrv ~]# cat /var/www/html/site?/index.html
<!DOCTYPE html><html><body>
<h1>Site 1</h1>
<p>Paragraphe inutile</p>
</body>
</html>

<!DOCTYPE html><html><body>
<h1>Site 2</h1>
<p>Paragraphe inutile</p>
</body>
</html>
```

la commande `'s/Titre nul/Site 1/'` permet de modifier directement en ligne de commande le fichier

## VirtualHost par IP

Aucun soucis à noter
```
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:ce:d5:af brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.98/24 brd 192.168.56.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet 192.168.56.97/24 scope global secondary eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fece:d5af/64 scope link 
       valid_lft forever preferred_lft foreve
```

## Les NameVirtualhost

J'ai rencontré un soucis à ce niveau.
Lors de la création des répertoires et des fichiers index.html, `systemctl restart httpd.service` a eu une erreur.
```
[root@websrv ~]# systemctl status httpd.service -l
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Fri 2019-10-04 16:20:38 UTC; 16min ago
     Docs: man:httpd(8)
           man:apachectl(8)
  Process: 2467 ExecStop=/bin/kill -WINCH ${MAINPID} (code=exited, status=1/FAILURE)
  Process: 27964 ExecReload=/usr/sbin/httpd $OPTIONS -k graceful (code=exited, status=0/SUCCESS)
  Process: 2466 ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND (code=exited, status=1/FAILURE)
 Main PID: 2466 (code=exited, status=1/FAILURE)

Oct 04 16:20:38 websrv systemd[1]: Starting The Apache HTTP Server...
Oct 04 16:20:38 websrv httpd[2466]: AH00526: Syntax error on line 1 of /etc/httpd/conf.d/site3.conf:
Oct 04 16:20:38 websrv httpd[2466]: Invalid command 'NameVirtalHost', perhaps misspelled or defined by a module not included in the server configuration
Oct 04 16:20:38 websrv systemd[1]: httpd.service: main process exited, code=exited, status=1/FAILURE
Oct 04 16:20:38 websrv kill[2467]: kill: cannot find process ""
Oct 04 16:20:38 websrv systemd[1]: httpd.service: control process exited, code=exited status=1
Oct 04 16:20:38 websrv systemd[1]: Failed to start The Apache HTTP Server.
Oct 04 16:20:38 websrv systemd[1]: Unit httpd.service entered failed state.
Oct 04 16:20:38 websrv systemd[1]: httpd.service failed.
```


J'ai utilisé la commande `journalctl -xe` :
```
-- Unit httpd.service has begun starting up.
Oct 04 16:54:25 websrv httpd[4463]: AH00526: Syntax error on line 1 of /etc/httpd/conf.d/site3.conf:
Oct 04 16:54:25 websrv httpd[4463]: Invalid command 'NameVirtalHost', perhaps misspelled or defined by a 
Oct 04 16:54:25 websrv systemd[1]: httpd.service: main process exited, code=exited, status=1/FAILURE
Oct 04 16:54:25 websrv kill[4464]: kill: cannot find process ""
Oct 04 16:54:25 websrv systemd[1]: httpd.service: control process exited, code=exited status=1
Oct 04 16:54:25 websrv systemd[1]: Failed to start The Apache HTTP Server.
-- Subject: Unit httpd.service has failed
-- Defined-By: systemd
```
Et donc c'est mon serveur Apache qui serait out, et j'arrive pas à le relancer pour l'instant même avec `sudo apachectl –k restart`
En relisant les logs, j'ai eu une erreur que j'ai corrigé en `/etc/httpd/conf.d/site3.conf` mais ça n'aide pas à redémarrer le serveur Apache.








*le vagrantfile est en retard...*