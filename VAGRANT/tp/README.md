# **TP VAGRANT**

## Création box VagrantFile

```
#configuration de la vagrant vm

Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"
  config.vm.box_check_update = false
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"
 end

#installation des pré-requis sur les vm (pas encore créer)

 config.vm.provision "shell", inline: <<-SHELL
        apt-get -qq update
        apt-get install -qq apache2
        apt-get install -qq libapache2-mod-php7.0
        apt-get install -qq php7.0-mbstring
        apt-get install -qq tree
        apt-get install -qq vim

#installation dokuwiki

        mkdir /opt/src
        wget -q -O /opt/src/dokuwiki.tgz https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz
        tar -zxf /opt/src/dokuwiki.tgz -C /var/www/html/
        mv /var/www/html/dokuwiki-2018-04-22b/ /var/www/html/dokuwiki/

#modification des droits et des users

        useradd -m -s /bin/bash -G www-data wiki

        tar -xf /vagrant/wiki-conf.tar -C /var/www/html/dokuwiki/conf
        chown -R wiki:www-data /var/www/html/
        chmod -R g+w /var/www/html/dokuwiki/data
        chmod -R g+w /var/www/html/dokuwiki/conf

#création d'un dossier wiki/.ssh pour créer une relation d'aprobation ssh

        mkdir /home/wiki/.ssh
        chown -R wiki:wiki /home/wiki/.ssh
        cp /vagrant/id_rsa /home/wiki/.ssh
        cat /vagrant/id_rsa.pub >> /home/wiki/.ssh/authorized_keys
        rm /etc/ssh/sshd_config
        cat /vagrant/sshd_config >> /etc/ssh/sshd_config
        chmod 700 /home/wiki/.ssh
        chmod 600 /home/wiki/.ssh/*

  SHELL

#création des vm et des leurs noms

  config.vm.define "master" do |master|
    master.vm.hostname = "master"
    master.vm.network "private_network", ip: "192.168.56.100"
 end

  config.vm.define "slave" do |backup|
   backup.vm.hostname = "slave"
   backup.vm.network "private_network", ip: "192.168.56.110"
 end
```